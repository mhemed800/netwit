--- 
title: "Post to the BLOG"
linkTitle: "Post to the BLOG"
type: docs
toc_hide: false
weight: 50
description: >
  Information on how to use this site as a community and incorporate your own adds, edits, and builds!
--- 

<body>


<div class="main" >
  <h2>Post a comment or suggestion about the site here</h2>
<div id="disqus_thread"></div>


<script>
var disqus_config = function () {
this.page.url = 'https://ura2j.gitlab.io/netwit/tenant/community/blog/';  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = '/netwit/tenant/community/blog/'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};


(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://srl-1.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            
<script id="dsq-count-scr" src="//srl-1.disqus.com/count.js" async></script>
</div>






