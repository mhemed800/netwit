---
title: "Information and Forms for Filing With the Court"
linkTitle: "Forms and Filing"
weight: 20
type: docs
menu:
  main:
    weight: 30
---


<details>

<summary>How do I get my landlord to fix something in my home that is broken because it threatens my health or safety? </summary> 

If there is something wrong with your apartment and your landlord has refused to fix it, you can go to your General District Court and file a **_Tenant’s Assertion_**. Once you do this whole process a judge can do one of the following options:

 - make your landlord fix the problem,

 - reduce your rent payments, 

 - hold onto your rent until the problem is fixed, OR

 - terminate your lease so that you can move out. 

Please go to the _Currently Renting_ section of this website to find out more about this process.


</details>

---
<details>

  <summary>Tools</summary> 


[Write Your Landlord a Letter and Fill Out A Form](https://lawhelpinteractive.org/Interview/GenerateInterview/7163/engine) 

[Videos](https://netwit.gitlab.io/hugo/va_info/ta-videos/) 

[Speak to the Community](/comunity)

</details>

--- 

Select a section below to find out more information 
