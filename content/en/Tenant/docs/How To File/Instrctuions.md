---
title: "Instructions For Tenants Assertion"
linkTitle: "Instructions"
type: docs
description: >
  Instructions for Tenants Assertion Form
---

## 1. First, download the Tenant’s Assertion and Complaint form. 

[Download the Form.](http://www.courts.state.va.us/forms/district/dc429.pdf)

You can fill this out on a computer or print it out and do it by hand. Take a look at the example [link] we have filled out for you to help guide you. Then, prepare yourself, as this process is slightly complicated. However, don’t worry. We will walk you through step by step.

## 2. Fill Out the Following Sections 


### Section 1—top left of the form

The first section asks you for the county or city where the property is located, and then the address of the court house. You can find the court’s address on Virginia’s find a court page. Click on the dropdown menu under “general district court”, select your city or county, and you will see the address in the middle of the page. Once you write the court’s address, you can ignore the rest of this section.

### Section 2 – right-hand column

In this column, write your name on the “plaintiff(s) – tenant(s)” line, and your landlord’s name on the “defendant(s) – landlord(s)”line. Then, write the address of the house or apartment where you live. 


### Section 3—middle left of the form

The third section asks you for information about your lease. fill this out to the best of your knowledge. Underneath that, it asks you to list the problems with your house or apartment. Look back at the letter you wrote your landlord to make sure you include everything. 

If your landlord is violating your written lease agreement, check the first box and write the provision of the lease on the line below. If there is mold in your apartment, check the second box and write “Va. Code § 55.1-220(5) (mold)” on the line below. For all other problems, check the third box, list the problem, and whether it affects your life, health, safety, or is a fire hazard.

The last part of this section asks what you would like the court to do to fix the problem. The court can 1) terminate the lease so you can move, 2) withhold your rent from your landlord until the problem is fixed, or 3) reduce your rent by a reasonable amount. for option 1, write “terminate lease” at the bottom of this section. For option two, write “rent payment into escrow”. For option three, write “rent reduction”. You may choose one, two, or all three of these options depending on what you would like your landlord to do to fix the problem. 

Finally, sign and date the bottom of this section. Good job—this is the most difficult part of the tenant’s assertion process. You can ignore the information at bottom of the page. Feel free to read the back of the form. It just tells you the information that we just gave you above!

## 3. Take The Next Steps 
 
### What happens next?

Now that you have a completed Tenant’s Assertion and Complaint form, you can take it to the court house. You even have the address-- it is written on the top of the form! When you get to the court house, go to the clerk of the court and tell him or her that you wish to file a Tenant’s Assertion. The clerk will ask for the form and may also ask for the return receipt from the landlord letter.

The clerk will then establish something called an “escrow account” for you. This is just a place where you can pay your rent to the judge so that you can be current on your rent while your landlord is fixing the problems. remember—you must be current on your rent, and you are required to pay the judge your rent within 5 days of the due date in your lease if your rent comes due again before the problems are fixed. 

The judge (?) will set a “Return Date” for you and the landlord to appear at court and argue your case, present evidence such as the pictures you took, and take account of damages. This hearing should occur within 15 days of but you can ask for an earlier date if it is an emergency.

[Where Should I File The Form](/where-to-file)

